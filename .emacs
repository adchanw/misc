(global-linum-mode 1)

(setq linum-format "%d ")

(setq-default indent-tabs-mode nil)
(infer-indentation-style)

(global-set-key (kbd "<C-tab>") 'comment-dwim)
(global-set-key (kbd "C-`") 'whitespace-mode)
(global-set-key (kbd "C-z") 'undo)

(setq-default frame-title-format
              '(:eval
                (format (buffer-name)
                        (cond 
                         (buffer-file-truename
                          (concat "(" buffer-file-truename ")"))
                         (dired-directory
                          (concat "{" dired-directory "}"))
                         (t
                          "[no file]")))))

(add-hook 'python-mode-hook
          (lambda ()
            (setq electric-indent-chars (delq ?: electric-indent-chars))))

(setq-default c-basic-offset 
